﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelSelector : MonoBehaviour
{
    //public string level;

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.gameObject.CompareTag("Player"))
    //    {
    //        SceneManager.LoadScene(level);
    //    }
    //}

    public void GoLevel1()
    {
        //Load Main scene
        SceneManager.LoadScene("Level 1");
    }
    public void GoLevel2()
    {
        //Load Main scene
        SceneManager.LoadScene("Level 2");
    }
    public void GoLevel3()
    {
        //Load Main scene
        SceneManager.LoadScene("Level 3");
    }
}
