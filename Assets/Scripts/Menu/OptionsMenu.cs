﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OptionsMenu : MonoBehaviour
{
    public GameObject optionsPanel;
    public GameObject playerPanel;

    public void OptionsPanel()
    {
        //Pause the game
        Time.timeScale = 0;

        //Show the panel
        optionsPanel.SetActive(true);
    }

    public void Return()
    {
        //continue the game
        Time.timeScale = 1;

        //hide the panel
        optionsPanel.SetActive(false);
    }

    public void GoMainMenu()
    {
        //continue game
        Time.timeScale = 1;

        //Load Main scene
        SceneManager.LoadScene("Menu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void SelectPlayer()
    {
        ///Show the panel
        playerPanel.SetActive(true);
        optionsPanel.SetActive(false);
    }
}
