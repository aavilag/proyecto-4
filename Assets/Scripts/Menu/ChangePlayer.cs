﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ChangePlayer : MonoBehaviour
{
    public GameObject playerPanel;
    public GameObject player;

    public void SetPlayerMonk()
    {
        PlayerPrefs.SetString("PlayerSelected", "Monk");
        ResetPlayerSkin();
    }

    public void SetPlayerLancer()
    {
        PlayerPrefs.SetString("PlayerSelected", "Lancer");
        ResetPlayerSkin();
    }

    public void SetPlayerShooter()
    {
        PlayerPrefs.SetString("PlayerSelected", "Shooter");
        ResetPlayerSkin();
    }

    public void SetPlayerMartial()
    {
        PlayerPrefs.SetString("PlayerSelected", "Martial");
        ResetPlayerSkin();
    }
    void ResetPlayerSkin()
    {
        Time.timeScale = 1;
        playerPanel.gameObject.SetActive(false);
        player.GetComponent<PlayerSelected>().ChangePlayerInMenu();
    }
}
