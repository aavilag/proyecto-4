﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class MenuController : MonoBehaviour
{
    public AudioSource track;
    //public GameObject playerPanel;
    public GameObject levelSelect;

    void Start()
    {
        track.loop = true;
        track.Play();
    }

    public void ContinueGame()
    {
        float checkpointX = PlayerPrefs.GetInt("checkpointX");
        float checkpointY = PlayerPrefs.GetInt("checkpointY");
        string level = PlayerPrefs.GetString("level");
    }

    public void PlayNewGame()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LevelSelector()
    {
        levelSelect.SetActive(true);
    }

    //public void SelectPlayer()
    //{
    //    //Show the panel
    //    playerPanel.SetActive(true);
    //}
}
