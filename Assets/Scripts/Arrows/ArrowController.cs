﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{
    public float speed = 8f;
    public float lifeTime = 20f;
    private int direction;
    private string fixedDirection;

    void Start()
    {
        Destroy(gameObject, lifeTime);
        System.Random rand = new System.Random();
        direction = rand.Next(0, 4);
    }

    void Update()
    {
        if (!String.IsNullOrEmpty(fixedDirection))
        {
            switch (fixedDirection)
            {
                case "d":
                    transform.Translate(Vector2.right * speed * Time.deltaTime);
                    break;
                case "l":
                    transform.Translate(Vector2.up * speed * Time.deltaTime);
                    break;
                case "r":
                    transform.Translate(Vector2.down * speed * Time.deltaTime);
                    break;
                case "u":
                    transform.Translate(Vector2.left * speed * Time.deltaTime);
                    break;
            }
        }
        else
        {
            switch (direction)
            {
                case 0:
                    transform.Translate(Vector2.right * speed * Time.deltaTime);
                    break;
                case 1:
                    transform.Translate(Vector2.up * speed * Time.deltaTime);
                    break;
                case 2:
                    transform.Translate(Vector2.down * speed * Time.deltaTime);
                    break;
                case 3:
                    transform.Translate(Vector2.left * speed * Time.deltaTime);
                    break;
            }
        }
    }

    public void SetFixedDirection(string fd)
    {
        this.fixedDirection = fd;
    }
}
