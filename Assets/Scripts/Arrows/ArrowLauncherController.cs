﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowLauncherController : MonoBehaviour
{
    private float waitTime;
    public float shootingRate = 2f;
    public Animator animator;
    public GameObject arrowPrefab;
    public Transform spawnArrowPoint;
    public string fixedDirection = "";

    void Start()
    {
        waitTime = shootingRate;
    }

    void Update()
    {
        if (waitTime <= 0)
        {
            waitTime = shootingRate;
            GameObject arrow;
            arrow = Instantiate(arrowPrefab, spawnArrowPoint.position, spawnArrowPoint.rotation);

            if (!String.IsNullOrEmpty(fixedDirection))
            {
                arrow.GetComponent<ArrowController>().SetFixedDirection(fixedDirection);
            }
        }
        else
        {
            waitTime -= Time.deltaTime;
        }
    }
}
