﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaformEffectorController : MonoBehaviour
{
    public PlatformEffector2D effector;
    public float waitTime;

    void Update()
    {
        if (Input.GetKey("down") || Input.GetKey("s"))
        {
            effector.rotationalOffset = 180f;
            Invoke("RestorePlatform", 1f);
        }
    }

    void RestorePlatform()
    {
        effector.rotationalOffset = 0f;
    }
}
