﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMove : MonoBehaviour
{
    public float speed = 1f;
    private int i;
    public Transform[] moveSpots;
    private bool isMovingUp;
    private bool isWaiting;

    void Start()
    {
        i = 0;
        isMovingUp = true;
        isWaiting = false;
    }

    void Update()
    {
        if (!isWaiting)
        {
            transform.position = Vector2.MoveTowards(transform.position, moveSpots[i].transform.position, speed * Time.deltaTime);

            if (Vector3.Distance(transform.position, moveSpots[i].transform.position) < 0.01)
            {
                if (i + 1 == moveSpots.Length) isMovingUp = false;
                if (i == 0) isMovingUp = true;

                if (isMovingUp) i++;
                else i--;

                StartCoroutine(wait());
                transform.position = Vector2.MoveTowards(transform.position, moveSpots[i].transform.position, speed * Time.deltaTime);
            }
        }
    }

    IEnumerator wait()
    {
        isWaiting = true;
        yield return new WaitForSeconds(1f);
        isWaiting = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.collider.transform.SetParent(transform);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        collision.collider.transform.SetParent(null);
    }
}
