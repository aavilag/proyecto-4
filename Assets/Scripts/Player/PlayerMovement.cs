﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    [SerializeField] float m_speed = 4.0f;
    [SerializeField] float m_jumpForce = 7.5f;

    public Animator m_animator;
    public Rigidbody2D m_body2d;
    private Sensor m_groundSensor;
    private Sensor m_wallSensorR1;
    private Sensor m_wallSensorR2;
    private Sensor m_wallSensorL1;
    private Sensor m_wallSensorL2;
    private bool m_grounded = false;
    private bool m_rolling = false;
    private int m_currentAttack = 0;
    private float m_timeSinceAttack = 0.0f;
    private float m_delayToIdle = 0.0f;
    private bool canJump = false;
    public float doubleJumpSpeed = 7f;

    void Start ()
    {
        m_groundSensor = transform.Find("GroundSensor").GetComponent<Sensor>();
        m_wallSensorR1 = transform.Find("WallSensor_R1").GetComponent<Sensor>();
        m_wallSensorR2 = transform.Find("WallSensor_R2").GetComponent<Sensor>();
        m_wallSensorL1 = transform.Find("WallSensor_L1").GetComponent<Sensor>();
        m_wallSensorL2 = transform.Find("WallSensor_L2").GetComponent<Sensor>();
    }

    void Update ()
    {
        // Increase timer that controls attack combo
        m_timeSinceAttack += Time.deltaTime;

        //Check if character just landed on the ground
        if (!m_grounded && m_groundSensor.State())
        {
            m_grounded = true;
            m_animator.SetBool("Grounded", m_grounded);
        }

        //Check if character just started falling
        if (m_grounded && !m_groundSensor.State())
        {
            m_grounded = false;
            m_animator.SetBool("Grounded", m_grounded);
        }

        // -- Handle input and movement --
        float inputX = Input.GetAxis("Horizontal");

        // Swap direction of sprite depending on walk direction
        if (inputX > 0)
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }
            
        else if (inputX < 0)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }

        // Move
        if (!m_rolling )
            m_body2d.velocity = new Vector2(inputX * m_speed, m_body2d.velocity.y);

        //Set AirSpeed in animator
        m_animator.SetFloat("AirSpeedY", m_body2d.velocity.y);

        //Crounch
        //if (Input.GetKey("down") || Input.GetKey("s"))
        //{
        //    m_animator.SetBool("Crounch", true);

        //}
        //else
        //{
        //    m_animator.SetBool("Crounch", false);
        //}

        //Attack
        if (Input.GetMouseButtonDown(0) && m_timeSinceAttack > 0.25f)
        {
            m_currentAttack++;

            // Loop back to one after third attack
            if (m_currentAttack > 2)
                m_currentAttack = 1;

            // Reset Attack combo if time since last attack is too large
            if (m_timeSinceAttack > 1.0f)
                m_currentAttack = 1;

            // Call one of three attack animations "Attack1", "Attack2", ...
            m_animator.SetTrigger("Attack" + m_currentAttack);

            // Reset timer
            m_timeSinceAttack = 0.0f;
        }

        // Crounch hit
        //else if (Input.GetMouseButtonDown(1) && m_grounded)
        //{
        //    //if (m_grounded == false)
        //    //{
        //    //    m_animator.SetTrigger("Attack4");
                
        //    //}
        //    //else
        //    //{
        //        m_animator.SetTrigger("Attack3");
        //        m_animator.SetBool("Crounch", true);
        //    //}
        //}

        //Jump
        if (Input.GetKey("up") || Input.GetKey("w"))
        {
            if (m_grounded)
            {
                m_animator.SetTrigger("Jump");
                m_grounded = false;
                m_animator.SetBool("Grounded", m_grounded);
                m_body2d.velocity = new Vector2(m_body2d.velocity.x, m_jumpForce);
                m_groundSensor.Disable(0.2f);
                canJump = true;
            }
           else 
            {
                if (Input.GetKeyDown("up") || Input.GetKeyDown("w")) { 
                    if (canJump)
                    {
                        m_body2d.velocity = new Vector2(m_body2d.velocity.x, doubleJumpSpeed);
                        m_animator.SetTrigger("Jump");
                        m_grounded = false;
                        m_animator.SetBool("Grounded", m_grounded);
                        m_groundSensor.Disable(0.2f);
                        canJump = false;
                    }
                }
            }
        }

        //Run
        else if (Mathf.Abs(inputX) > Mathf.Epsilon)
        {
            // Reset timer
            m_delayToIdle = 0.05f;
            m_animator.SetInteger("AnimState", 1);
        }

        //Idle
        else
        {
            // Prevents flickering transitions to idle
            m_delayToIdle -= Time.deltaTime;
                if(m_delayToIdle < 0)
                    m_animator.SetInteger("AnimState", 0);
        }


    }

}
