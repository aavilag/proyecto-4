﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerRespawn : MonoBehaviour
{
    private float checkpointX, checkpointY;
    public Animator animator;

    void Start()
    {
        float x = gameObject.transform.position.x;
        float y = gameObject.transform.position.y;
        PlayerPrefs.SetFloat("checkpointX", x);
        PlayerPrefs.SetFloat("checkpointY", y);
    }

    public void CheckpointReached(float x, float y)
    {
        PlayerPrefs.SetFloat("checkpointX", x);
        PlayerPrefs.SetFloat("checkpointY", y);
    }

    public void PlayerDamage()
    {
        animator.Play("Hit");
        Invoke("ReturnToCheckpoint", 0.5f);
    }

    void ReturnToCheckpoint()
    {
        float chX = PlayerPrefs.GetFloat("checkpointX");
        float chY = PlayerPrefs.GetFloat("checkpointY");
        animator.Play("Idle");
        if (chX != 0 && chY != 0)
            transform.position = new Vector2(chX, chY);
    }
}
