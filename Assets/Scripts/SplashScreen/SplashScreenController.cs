﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreenController : MonoBehaviour
{
    public static int sceneNumber;
    public AudioSource track;

    void Start()
    {
        track.Play();
        if (sceneNumber == 0)
        {
            StartCoroutine(ToMenu());

        }
    }

    IEnumerator ToMenu ()
    {
        yield return new WaitForSeconds(5);
        sceneNumber = 1;
        SceneManager.LoadScene(1);
    }
}
