﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantEnemy : MonoBehaviour
{
    private float waitedTime;
    public float waitTimeoAttack = 3;
    public Animator animator;
    public GameObject bulletPrefab;
    public Transform lauchSpawnPoint;

    void Start()
    {
        waitedTime = waitTimeoAttack;
    }

    void Update()
    {
        if (waitedTime<=0)
        {
            waitedTime = waitTimeoAttack;
            animator.Play("Attack");
            Invoke("LaunchBullet", 0.5f);
        }
        else
        {
            waitedTime -= Time.deltaTime;
        }
    }

    public void LaunchBullet() 
    {
        GameObject newBullet;
        newBullet = Instantiate(bulletPrefab, lauchSpawnPoint.position, lauchSpawnPoint.rotation);
    }
}
