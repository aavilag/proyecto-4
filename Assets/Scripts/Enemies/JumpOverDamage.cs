﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpOverDamage : MonoBehaviour
{
    public Collider2D co2D;
    public Animator animator;
    public SpriteRenderer spriteRenderer;
    public GameObject destroyParticle;
    public float jumpForce = 9f;
    public int lifes = 1;
    public AudioSource deadSound;
    public bool isBoss = false;
    public GameObject[] bossStars = new GameObject[10];

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.up * jumpForce;
            getHit();
            isStillAlive();
        }
    }

    public void getHit()
    {
        lifes--;
       // animator.Play("Dead");
    }

    public void isStillAlive()
    {
        if (lifes == 0)
        {
            deadSound.Play();
            destroyParticle.SetActive(true);
            spriteRenderer.enabled = false;
            Invoke("Die", 0.4f);
        }
    }

    public void Die()
    {
        if (isBoss)
        {
            bossStars[0].SetActive(true);
            bossStars[1].SetActive(true);
            bossStars[2].SetActive(true);
            bossStars[3].SetActive(true);
            bossStars[4].SetActive(true);
            bossStars[5].SetActive(true);
            bossStars[6].SetActive(true);
            bossStars[7].SetActive(true);
            bossStars[8].SetActive(true);
            bossStars[9].SetActive(true);
        }
        Destroy(gameObject);
    }
}
