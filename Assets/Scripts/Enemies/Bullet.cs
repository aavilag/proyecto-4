﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPlant : MonoBehaviour
{
    public float speed = 4;
    public float lifeTime = 2;
    public bool left = true;

    void Start()
    {
        Destroy(gameObject, lifeTime);
    }

    void Update()
    {
        if (left == true)
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        }
    }
}
