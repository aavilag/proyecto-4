﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMovement : MonoBehaviour
{
    public Animator animator;
    public SpriteRenderer spriteRenderer;
    private Vector2 currentPos;
    public float speed = 1f;
    private int i = 0;
    public Transform[] moveSpots = new Transform[2];
    private bool isMovingUp = true;
    private bool isWaiting = false;
    public float waitTime = 3f;
    public bool isSpriteLookingAtRight = false;
    public bool isFlyingEnemy = false;
    private bool isFollowingPlayer = false;
    public GameObject player;

    void Update()
    {
        if (!isWaiting && !isFollowingPlayer)
        {
            if (!isFlyingEnemy) animator.SetBool("Walk", true);
            if (isFlyingEnemy) animator.SetBool("Attack", false);

            if (isSpriteLookingAtRight)
            {
                if (i == 0) spriteRenderer.flipX = true;
                else spriteRenderer.flipX = false;
            }
            else
            {
                if (i == 0) spriteRenderer.flipX = false;
                else spriteRenderer.flipX = true;
            }
            transform.position = Vector2.MoveTowards(transform.position, moveSpots[i].transform.position, speed * Time.deltaTime);
            if (Vector3.Distance(transform.position, moveSpots[i].transform.position) < 0.01)
            {
                if (i + 1 == moveSpots.Length) isMovingUp = false;
                if (i == 0) isMovingUp = true;
                if (isMovingUp) i++;
                else i--;
                StartCoroutine(wait());
            }
        }

        if (isFollowingPlayer)
        {
            if (player)
            {
                if (isFlyingEnemy) animator.SetBool("Attack", true);
                transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
            }
        }
    }

    IEnumerator wait()
    {
        isWaiting = true;
        if (!isFlyingEnemy) animator.SetBool("Walk", false);
        yield return new WaitForSeconds(waitTime);
        isWaiting = false;
    }

    public void setIsFollowingPlayer()
    {
        this.isFollowingPlayer = !this.isFollowingPlayer;
    }
}
