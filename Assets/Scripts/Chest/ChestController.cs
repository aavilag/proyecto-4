﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestController : MonoBehaviour
{
    public AudioSource openedSound;
    private bool chestOpened = false;
    public GameObject[] stars = new GameObject[6];

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (!chestOpened)
            {
                chestOpened = true;
                openedSound.Play();
                GetComponent<SpriteRenderer>().enabled = false;
                stars[0].SetActive(true);
                stars[1].SetActive(true);
                stars[2].SetActive(true);
                stars[3].SetActive(true);
                stars[4].SetActive(true);
                stars[5].SetActive(true);
            }

        }
    }
}
