﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarCollected : MonoBehaviour
{
    public AudioSource collectedSound;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            transform.parent.GetComponent<StarController>().IncreseCount();
            collectedSound.Play();
            GetComponent<SpriteRenderer>().enabled = false;
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
            Destroy(gameObject, 0.5f);
        }
    }
}
