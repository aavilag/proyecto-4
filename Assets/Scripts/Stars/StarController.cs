﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StarController : MonoBehaviour
{
    public Text text;
    private int total;
    private int count;
    public string nextScene;
    public int starsToWin;
    public GameObject completed;

    void Start()
    {
        total = transform.childCount;
        count = 0;
    }

    void Update()
    {
        text.text = count.ToString();
        if (count == starsToWin)
        {
            completed.SetActive(true);
            StartCoroutine(ChangeScene());
        }
    }

    IEnumerator ChangeScene()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(nextScene);
    }

    public void IncreseCount()
    {
        count++;
    }
}
